"""
This program uses vosk models and the Kaldi Recognizer to transcribe audio
for use with zoom captioning api and or saving a text transcript.

This file named gui.py is the entry point for the zoom captions program
Written by Bryan Chang in Python 3.11 starting August 2022 to April 2023
"""


# Import built in modules
import os
import sys
import time
from threading import Thread

# Import PyQt6 Modules from external library
from PyQt6 import QtGui, QtWidgets
from PyQt6 import uic
from PyQt6.QtWidgets import QFileDialog, QGraphicsPixmapItem
import sklearn


# Import external Vosk library module
from vosk import Model

# Import local program modules and classes
from src.settings import current_config
from src.threads import Update_Textbox
from src.speech_to_text_backend import Stt_Thread




def main():
    # Create QApplication instance named app
    app = QtWidgets.QApplication(sys.argv)

    # load UI from a file
    print("current dir: ",os.path.curdir)
    print("file path: ", (__file__))
    window = uic.loadUi(os.path.join(os.path.dirname(__file__),"mainwindow.ui"))

    # create long-running stoppable logger thread as logger
    logger = Update_Textbox(window)
    logger.start()


    # Instantiate stt recognizer thread in the global scope
    stt_recognizer: Stt_Thread = None


    # define callback functions to be connected to UI elements
    def start_transcription_clicked():
        """Create long-running stoppable speech-to-text recognizer thread as stt_recognizer.
        Starts transcriber thread passing the input device and model
        and calls a function to disable settings controls"""
        global stt_recognizer
        stt_recognizer = Stt_Thread(current_config.current_model, current_config.input_device)
        stt_recognizer.start()
        print_to_text_box("Transcription Started")
        controls_disabler()


    def stop_stt():
        """This is the callback function for the 'stop transcription' push button.
        It stops and joins the recognizer thread and also calls a function to re-enable the settings controls"""
        global stt_recognizer
        stt_recognizer.stop()
        stt_recognizer.join()
        stt_recognizer = None
        print_to_text_box("Transcription Stopped")
        controls_enabler()


    def stop_logger():
        """Function for closing the logger thread, called before closing by the exit handler"""
        logger.stop()
        logger.join()


    def controls_disabler():
        """Disables setting controls in the UI for the user while the program is running,
        also updates running status indicator"""
        window.toolButton.setIcon(QtGui.QIcon("icons/green.png"))
        window.running_label.setText("Transcriber is running")
        window.save_transcript_checkBox.setEnabled(False)
        window.model_comboBox.setEnabled(False)
        window.input_lang_comboBox.setEnabled(False)
        window.output_lang_comboBox.setEnabled(False)
        window.input_device_comboBox.setEnabled(False)
        window.start_transcription_pushButton.setEnabled(False)
        window.save_to_dir_pushButton.setEnabled(False)
        window.stop_transcription_pushButton.setEnabled(True)
        window.api_key_text_input.setEnabled(False)


    def controls_enabler():
        """Re-enables the settings controls in the UI and updates the running status indicator."""
        window.toolButton.setIcon(QtGui.QIcon("icons/red.png"))
        window.running_label.setText("Transcriber is not running")
        window.save_transcript_checkBox.setEnabled(True)
        window.model_comboBox.setEnabled(True)
        window.input_lang_comboBox.setEnabled(True)
        window.output_lang_comboBox.setEnabled(True)
        window.input_device_comboBox.setEnabled(True)
        window.start_transcription_pushButton.setEnabled(True)
        window.save_to_dir_pushButton.setEnabled(True)
        window.stop_transcription_pushButton.setEnabled(False)
        window.api_key_text_input.setEnabled(True)


    def save_api_key():
        """Callback function for updating the API key"""
        current_config.API_token = window.api_key_text_input.text()


    def save_text_transcription():
        """Call back function for the save transcription checkbox"""
        if window.save_transcript_checkBox.isChecked():
            current_config.text_flag = True
            transcription_on_message = ("Transcription will be saved to the folder: " + current_config.data_folder
                                        + " as a text file named: " + current_config.filename + "\n")
            print(transcription_on_message)
            print_to_text_box(transcription_on_message)

        else:
            current_config.text_flag = False
            print("Warning! Transcription saving disabled!")
            print_to_text_box("Warning! Transcription saving disabled!")


    def save_transcript_to():
        """Call back for save to push button that opens a file directory browser for choosing a directory."""
        file_filter = 'Directory'
        returned_data_folder = QFileDialog.getExistingDirectory(
            window,
            'select folder',
            current_config.data_folder
        )
        if returned_data_folder:
            current_config.data_folder = returned_data_folder
            print_to_text_box("Transcript save folder changed to " + current_config.data_folder)
            return
        else:
            return


    def model_change():
        """Linear thread target function that changes and load the new model"""
        print("Model will be changed to ", window.model_comboBox.currentText(), "please wait...")
        print_to_text_box("Model will be changed to " + window.model_comboBox.currentText() + " please wait...")
        current_config.current_model = Model(model_path=os.path.join("model", window.input_lang_comboBox.currentText(), window.model_comboBox.currentText()))
        print("Model successfully changed to", window.model_comboBox.currentText())
        print_to_text_box("Model successfully changed to " + window.model_comboBox.currentText())


    def model_changed(self):
        """Callback function for changing the language recognition model using a thread call"""
        print("test", window.model_comboBox.currentText())
        if window.model_comboBox.currentText() != "":
            model_thread = Thread(target=model_change).start()
        return


    def input_language_changed():
        """Callback function for updating the input language"""
        current_config.input_lang = current_config.languages.get(window.input_lang_comboBox.currentText())
        print("Input language changed to", window.input_lang_comboBox.currentText())
        window.model_comboBox.clear()
        model_list = [d for d in os.listdir("model/"+window.input_lang_comboBox.currentText())]

        for each in model_list:
            window.model_comboBox.addItem(each)
            if "small" in each:
                window.model_comboBox.setCurrentText(each)


    def output_language_changed():
        """Callback function for updating the output language"""
        current_config.output_lang = current_config.languages.get(window.output_lang_comboBox.currentText())
        print("Output language changed to", window.output_lang_comboBox.currentText())

    def input_device_changed():
        """Callback for updating input device"""
        current_config.input_device = window.input_device_comboBox.currentText()


    def scroll_text_box():
        """Callback function for scrolling the text box down as new text appears"""
        scrollbar = window.output_log_textEdit.verticalScrollBar()
        bottom = scrollbar.maximum()
        scrollbar.setValue(bottom)


    def print_to_text_box(text):
        """Function for printing something to the text box log widget with a time stamp"""
        window.output_log_textEdit.append(time.strftime("[%H:%M:%S]") + "\n" + text + "\n\n")


    def my_exit_handler():
        """"Custom exit handler that closes threads when closing application window"""
        logger.stop()
        logger.join()
        stt_recognizer.stop()
        stt_recognizer.join()
        return

    # Populate combo boxes in UI and set default values

    # Populate model selector combo box with models in specified model folder
    for each in os.listdir("model/English"):
        window.model_comboBox.addItem(each)

    # Populate input language combo box with languages with folders in model folder
    for each in os.listdir("model"):
        window.input_lang_comboBox.addItem(each)

    # Set combobox to model default: small english for quickest start up time
    window.model_comboBox.setCurrentText("small_model")

    # Populate output device combo box with each input device found by the program
    for index, name in current_config.input_device_list:
        window.input_device_comboBox.addItem(name)

    # Disable transcription button to start
    window.stop_transcription_pushButton.setEnabled(False)

    # Set status icon to red
    window.toolButton.setIcon(QtGui.QIcon(os.path.join(os.path.dirname(__file__), "icons", "red.png")))

    # Set the window icon to logo
    window.setWindowIcon(QtGui.QIcon(os.path.join(os.path.dirname(__file__), "logo", "logo_icon.svg")))

    # prints opening message to text edit log
    window.output_log_textEdit.setPlainText("Log text will appear here:\n")


    # Binding UI element signals to callback functions
    window.start_transcription_pushButton.clicked.connect(start_transcription_clicked)  # Start Transcription button pushed

    window.api_key_text_input.editingFinished.connect(save_api_key)  # API key text field Text added

    window.stop_transcription_pushButton.clicked.connect(stop_stt)  # Stop Transcription button pushed

    window.save_transcript_checkBox.clicked.connect(save_text_transcription)  # Saved transcript check box changed

    window.input_lang_comboBox.currentTextChanged.connect(input_language_changed)  # Output language combo boc

    window.output_lang_comboBox.currentTextChanged.connect(output_language_changed)  # Output language combo box

    window.save_to_dir_pushButton.clicked.connect(save_transcript_to)  # File explore of save transcript to button

    window.model_comboBox.currentTextChanged.connect(model_changed)  # Model selector combo box

    window.output_log_textEdit.textChanged.connect(scroll_text_box)  # Calls scroll text callback function to scroll when
    # text is added

    window.input_device_comboBox.currentTextChanged.connect(input_device_changed)  # Update input device

    app.aboutToQuit.connect(my_exit_handler)    # Connect exit event to custom function

    window.show()   # Show app window

    app.exec()  # Start QT application event loop

if __name__=='__main__':
    main()