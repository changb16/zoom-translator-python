import os.path
import time
from vosk import Model
from queue import Queue
import sounddevice as sd


# Method to list of Input devices
def input_devices():
    dl = sd.query_devices()
    return [(d['index'], d['name']) for d in dl if d['max_input_channels']]


"""Create object to contain settings"""


class Config:
    def __init__(self):
        """Settings and default settings for all the program"""

        self.languages = dict(

            German="de-DE",
            English="en-US",
            Japanese="jp-JP",
            Spanish="es-ES",
            French="fr-FR",
            Chinese="zh-CN",
            Korean="ko-KO"
        )

        # Language that the program is expecting to hear
        self.input_lang = self.languages.get("English")

        # Language that the program is outputting the subtitles in
        self.output_lang = self.languages.get("English")

        # Token for zoom caption API
        self._API_token = ""

        # on/off save transcript
        self.save_transcript = True

        # Selected input Device
        self.input_device = 0

        self.input_device_list = input_devices()

        # Recognition Model, default is small english model
        self.current_model = Model(model_path="model/English/small_model")

        # Flag for text transcription
        self.text_flag = False

        # Desired File path for transcription text file
        self.data_folder = os.path.expanduser(os.path.join('~', 'documents', 'transcripts'))
        if not os.path.isdir(self.data_folder):
            os.mkdir(self.data_folder)
            print("folder created : ", self.data_folder)

        self.filename = time.strftime("%Y%m%d-%H%M%S.txt")

        self.log = Queue()



    # Getter function
    @property
    def API_token(self):
        return self._API_token

    # API key Setter
    @API_token.setter
    def API_token(self, val=None):
        if val == None:
            val = input("Please enter api key for zoom meeting")
        self._API_token = val




# Instantiate instance of settings class
# Load default settings
current_config = Config()
