"""
This module named speech_to_text_backend.py implements speech text functionality using the Vosk library for
real-time transcription of an audio stream from an input device. The transcribed text is then processed
and if specified, translated and logged for storage.
Additionally, the module can interface with Zoom and send real-time captions during a Zoom meeting.
The Stt_Thread class inherits from the Stoppable_Thread class and uses the KaldiRecognizer class to perform
the speech recognition. The program also utilizes threading for concurrent execution of certain tasks.

Program written by Bryan Chang Python 3.11 in from late 2022 - early 2023
A portion of this module was adapted and modified from opensource code shared on Alpha Cephei's Vosk GitHub repository.
"""

# Import required default libraries
import sys
import json
from threading import Thread
import time
import os
from queue import Queue

# Import external libraries
import sounddevice as sd
from vosk import Model, KaldiRecognizer
from translate import Translator
from profanity_check import predict

# Import local program modules and classes
from src.zoom_caption_util import send_to_zoom
from src.settings import current_config
from src.threads import Stoppable_Thread


class Stt_Thread(Stoppable_Thread):
    """This class inherits Stoppable_Thread allowing it to be closed gracefully as needed.
    It receives model and device as arguments for the kaldi recognizer and input stream respectively.
    Runs in a loop after the user presses start transcription and exits the loop and terminates the thread when
    stop transcription is pressed of the application window is closed.
    """
    def __init__(self, model, device=0, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model
        self.device = device

    @property
    def device(self):
        return self._device

    @device.setter
    def device(self, value):
        self._device = value
        device_info = sd.query_devices(value, "input")
        self.samplerate = int(device_info["default_samplerate"])

    def write_to_transcript(self, result):
        """Callback function for writing recognized speech to transcript"""
        filename = time.strftime("%Y%m%d-%H%M%S.txt")
        current_config.filename = filename
        filepath = os.path.join(current_config.data_folder, filename)
        with open(filepath, "w") as f:
            f.write(result)

    @staticmethod
    def print_stop():
        print("Transcription Stopped")

    @staticmethod
    def profanity_filter(sentence):
        words = []
        words = sentence.split(" ")
        for i in range(len(words)):
            swear = predict([words[i]])
            if swear:
                words[i] = "#*$@*"
        sentence = ' '.join(words)
        return sentence

    def run(self):

        # Create a queue named q
        q = Queue()

        def callback(indata, frames, time, status):
            """This is called (from a separate thread) for each audio block.
            It put the audio data it gets in to the queue"""
            if status:
                print(status, file=sys.stderr)
            q.put(bytes(indata))

        # Create sounddevice input stream instance
        with sd.RawInputStream(samplerate=self.samplerate, blocksize=8000, device=self.device,
                               dtype="int16", channels=1, callback=callback):
            print("#" * 80)
            print("Transcription started")
            print("#" * 80)

            # Create an instance of recognizer as rec
            rec = KaldiRecognizer(self.model, self.samplerate)

            # Run main recognizer loop until user sends stop signal via the GUI
            self.is_running = True
            while self.is_running:

                # Get audio data from queue
                data = q.get()

                # Returns true if the recognizers recognizes speech in the audio
                if rec.AcceptWaveform(data):
                    result = json.loads(rec.Result()).get("text").encode("utf-8").decode("utf-8")

                    # Checks for text to process
                    if result:

                        # Run text through profanity filter for sanitization
                        if predict([result]):
                            result = self.profanity_filter(result)

                        # Translate the text to the desired language
                        translator = Translator(from_lang=current_config.input_lang, to_lang=current_config.output_lang)
                        result = translator.translate(result)

                        # If a zoom caption API is given, connect to zoom meeting
                        if current_config.API_token:
                            """If there is a zoom api token entered, run thread to send captions to zoom"""
                            Thread(target=send_to_zoom, args=[current_config.API_token, result]).start()

                        # Format result with time stamps for transcription and output log
                        format_result = (time.strftime("[%H:%M:%S]") + "\n" + result + "\n\n")
                        current_config.log.put(format_result)

                        # Write to transcript if user indicated to in GUI
                        if current_config.text_flag:
                            Thread(target=self.write_to_transcript, args=(format_result,)).start()
        return
