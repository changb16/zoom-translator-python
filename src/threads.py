"""
This module provides a framework for running a thread that periodically updates the textbox element with new
messages, and that can be stopped gracefully when desired.
This module Subclasses the Thread class to create a Stoppable_Thread class.
It also subclasses the Stoppable_Thread class to create the update_textbox class.

Module written by Bryan Chang Python 3.11 in from late 2022 - early 2023 as part of the Zoom Transcriber program
"""

# Import required libraries
from threading import Thread
from src.settings import current_config


class Stoppable_Thread(Thread):
    """Subclass of Thread that can be ended gracefully when desired."""
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)

        self.is_running = False

    def stop(self):
        """Method toggles the running flag to end the loop allowing the thread to end gracefully"""
        self.is_running = False


class Update_Textbox(Stoppable_Thread):
    """Subclass of Stoppable_Thread that updates the text edit GUI element which serves as a text log"""
    def __init__(self, window, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.window = window

    def run(self):
        """Method starts the loop and gets text from the log queue
         and prints it to the GUI log while the flag is true"""
        print("Starting logger")
        self.is_running = True
        while self.is_running:
            message = current_config.log.get()
            self.window.output_log_textEdit.append(message)
        return




