"""
This module contains the methods for sending the transcribed speech text into a Zoom meeting via the caption API
Module written by Bryan Chang Python 3.11 in from late 2022 - early 2023 as part of the Zoom Transcriber program
"""
# Import required libraries
import requests

def get_seq_num(token):
    """Function to get expected sequence number from zoom"""
    split_token = token.split("?", 1)
    print(split_token)
    split_token[0] += '/seq'
    token = "?".join(split_token)
    print(token)
    seq_num=0
    try:
        response = requests.get(token)
        if response.status_code== 200:
            seq_num = int(response.content.decode("utf-8"))
    except Exception as e:
        print("error bad response")
        print(e)
    return seq_num


def send_to_zoom(token=None, caption_text=None):
    # DEBUG Print token
    print(token)
    global zoom_caption_queue
    # Get current seq number
    # Set zoom caption sequence to current expected zoom sequence number
    seq_num = get_seq_num(token)
    print(seq_num)
    # Do post requests
    print(caption_text)
    # If input language not equal output language

    if caption_text:
        seq_num += 1
        print(caption_text)
        # FIX
        url = f'{token}&seq={seq_num}&lang=en-US'
        print(url)
        data = caption_text.encode("utf-8")
        response = requests.post(url, data)
        print(response)
